FROM nginx:latest
RUN rm /etc/nginx/conf.d/default.conf
RUN echo "server { listen 80; location / { return 200 'nginx is running!'; } location /nordsecurity { return 418; }}" > /etc/nginx/conf.d/default.conf

